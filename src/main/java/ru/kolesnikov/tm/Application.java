package ru.kolesnikov.tm;

import ru.kolesnikov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.EXIT: exit(); break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Artem Kolesnikov");
        System.out.println("E-MAIL: tema58-rus@yandex.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show version info.");
        System.out.println(TerminalConst.HELP + " - Display terminal commands.");
        System.out.println(TerminalConst.EXIT + " - Close application.");
    }

}
